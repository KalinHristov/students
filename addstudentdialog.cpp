#include "addstudentdialog.h"

#include <QGridLayout>
#include <QFileDialog>

AddStudentDialog::AddStudentDialog(QWidget *parent)
    : QDialog(parent)
{
    m_avatarLabel = new QLabel("Avatar:");
    m_firstNameLabel = new QLabel("First name:");
    m_surnameLabel = new QLabel("Surname:");
    m_lastNameLabel = new QLabel("Last name:");
    m_fNLabel = new QLabel("FN:");
    m_courseLabel = new QLabel("Course:");

    m_firstNameText = new QLineEdit;
    m_surnameText = new QLineEdit;
    m_lastNameText = new QLineEdit;
    m_fNText = new QLineEdit;
    m_courseText = new QLineEdit;

    m_selectAvatar = new QPushButton("Select avatar");
    m_okButton = new QPushButton("OK");
    m_okButton->setDisabled(true);
    m_cancelButton = new QPushButton("Cancel");

    QGridLayout* gLayout = new QGridLayout;


    gLayout->addWidget(m_firstNameLabel, 0, 0);
    gLayout->addWidget(m_firstNameText, 0, 1);

    gLayout->addWidget(m_surnameLabel, 1, 0);
    gLayout->addWidget(m_surnameText, 1, 1);

    gLayout->addWidget(m_lastNameLabel, 2, 0);
    gLayout->addWidget(m_lastNameText, 2, 1);

    gLayout->addWidget(m_fNLabel, 3, 0);
    gLayout->addWidget(m_fNText, 3, 1);

    gLayout->addWidget(m_courseLabel, 4, 0);
    gLayout->addWidget(m_courseText, 4, 1);

    gLayout->addWidget(m_selectAvatar, 5, 1);

    connect(m_selectAvatar, SIGNAL(clicked()),
            this, SLOT(selectAvatar()));

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(m_okButton);
    buttonLayout->addWidget(m_cancelButton);

    gLayout->addLayout(buttonLayout, 6, 1, Qt::AlignRight);

    connect(m_okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(m_cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(gLayout);
    setLayout(mainLayout);

    connect(m_firstNameText, SIGNAL(textChanged(QString)), this, SLOT(checkInformation()));
    connect(m_surnameText, SIGNAL(textChanged(QString)), this, SLOT(checkInformation()));
    connect(m_lastNameText, SIGNAL(textChanged(QString)), this, SLOT(checkInformation()));
    connect(m_fNText, SIGNAL(textChanged(QString)), this, SLOT(checkInformation()));
    connect(m_courseText, SIGNAL(textChanged(QString)), this, SLOT(checkInformation()));

    setWindowTitle(tr("Add a Student"));
}

Student* AddStudentDialog::student()
{
    Student* student = new Student(m_avatar, m_firstNameText->text(), m_surnameText->text(), m_lastNameText->text(), m_fNText->text(), m_courseText->text().toInt());

    return student;
}

void AddStudentDialog::selectAvatar()
{
    QString filter = "File Description (*.png *.jpg *.bmp)";
    m_avatar =  QFileDialog::getOpenFileName(this, "Select Avatar", QDir::homePath(), filter);
    checkInformation();
}

void AddStudentDialog::checkInformation()
{
    if(m_avatar.size() > 0
            && m_firstNameText->text().size() > 0
            && m_surnameText->text().size() > 0
            && m_lastNameText->text().size() > 0
            && m_fNText->text().size() > 0
            && m_courseText->text().size() > 0)
    {
        m_okButton->setEnabled(true);
    }
    else
    {
        m_okButton->setDisabled(true);
    }
}
