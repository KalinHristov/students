#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "studentstablemodel.h"

#include <QWidget>
#include <QTableView>
#include <QPushButton>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget* parent = Q_NULLPTR );

public slots:
    void addStudent();
    void deleteStudent();
    void save();
    void open();

private:
    StudentsTableModel* m_studentTable;
    QTableView* m_tableView;
    QPushButton* m_addStudent;
    QPushButton* m_deleteStudent;
    QPushButton* m_save;
    QPushButton* m_open;

    void buttonStyle(QPushButton* button, QString path);
};

#endif // MAINWINDOW_H
