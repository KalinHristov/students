#include "mainwindow.h"
#include "addstudentdialog.h"
#include "student.h"

#include <QTableView>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QHBoxLayout>
#include <QWidget>
#include <QGridLayout>
#include <QDir>
#include <QFileDialog>
#include <QtCore>
#include <QSize>

MainWindow::MainWindow( QWidget* parent )
    : QWidget( parent )
    , m_studentTable(Q_NULLPTR)
    , m_tableView(Q_NULLPTR)
    , m_addStudent(Q_NULLPTR)
    , m_deleteStudent(Q_NULLPTR)
    , m_save(Q_NULLPTR)
    , m_open(Q_NULLPTR)
{
    m_studentTable = new StudentsTableModel;

    m_tableView = new QTableView;
    m_tableView->setModel( m_studentTable );
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->verticalHeader()->hide();
    m_tableView->setIconSize(QSize(30, 30));

    m_addStudent = new QPushButton();
    buttonStyle(m_addStudent, "plus_icon.png");
    m_deleteStudent = new QPushButton();
    buttonStyle(m_deleteStudent, "minus_icon.png");
    m_save = new QPushButton();
    buttonStyle(m_save, "save_icon.png");
    m_open = new QPushButton();
    buttonStyle(m_open, "folder_icon.png");

    connect(m_addStudent, SIGNAL(clicked()),
            this, SLOT(addStudent()));
    connect(m_deleteStudent, SIGNAL(clicked()),
            this, SLOT(deleteStudent()));
    connect(m_save, SIGNAL(clicked()),
            this, SLOT(save()));
    connect(m_open, SIGNAL(clicked()),
            this, SLOT(open()));

    QGridLayout* gLayout = new QGridLayout;
    QHBoxLayout* buttons = new QHBoxLayout();
    buttons->addWidget(m_addStudent);
    buttons->addWidget(m_deleteStudent);
    buttons->addWidget(m_save);
    buttons->addWidget(m_open);

    gLayout->addLayout(buttons, 0, 1, Qt::AlignRight);
    QVBoxLayout* const mainLayout = new QVBoxLayout( this );
    mainLayout->addWidget(m_tableView);
    mainLayout->addLayout(gLayout);
}

void MainWindow::addStudent()
{
    AddStudentDialog addStudentDialog;
    if (addStudentDialog.exec())
    {
        m_studentTable->addStudent(addStudentDialog.student());
    }
}

void MainWindow::deleteStudent()
{
    QModelIndexList rows = m_tableView->selectionModel()->selectedRows();
    m_studentTable->removeSelectedStudents(rows);
}

void MainWindow::save()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Save file", QDir::homePath());
    m_studentTable->saveFile(filePath);
}

void MainWindow::open()
{
    //QString filter = "File Description (*.xml)";
    QString filePath = QFileDialog::getOpenFileName(this, "Open file", QDir::homePath());
    m_studentTable->openFile(filePath);
}

void MainWindow::buttonStyle(QPushButton* button, QString path)
{
    QDir directory(":/icons");

    button->setIcon(QIcon(directory.filePath(path)));
    button->setStyleSheet("border-style: none;");
    button->setIconSize(QSize(32,32));
}
