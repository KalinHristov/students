#include "mainwindow.h"

#include <QApplication>
#include <QTableView>
#include <QtCore>
#include <QtGui>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow mainWindow;
    mainWindow.show();

    const int result = app.exec();

    return result;
}
