QT += core gui
QT += widgets
SOURCES += \
    main.cpp \
    mainwindow.cpp \
    student.cpp \
    studentstablemodel.cpp \
    filecontroller.cpp \
    addstudentdialog.cpp

HEADERS += \
    mainwindow.h \
    student.h \
    studentstablemodel.h \
    filecontroller.h \
    addstudentdialog.h

RESOURCES += \
    files.qrc
